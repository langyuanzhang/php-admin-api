﻿# Host: 127.0.0.1  (Version 8.0.12)
# Date: 2019-04-22 17:19:52
# Generator: MySQL-Front 6.1  (Build 1.21)


#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(32) DEFAULT NULL COMMENT '用户登陆名',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `nickname` char(32) DEFAULT '快乐每一天' COMMENT '昵称',
  `state` tinyint(3) DEFAULT '1' COMMENT '状态1=正常，0=待审核，-1=关闭',
  `group` tinyint(3) DEFAULT '0' COMMENT '权限组',
  `create_time` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) unsigned DEFAULT NULL COMMENT '修改时间',
  `extend` varchar(255) DEFAULT NULL COMMENT '拓展1',
  `headimg` varchar(255) DEFAULT NULL COMMENT '头像',
  `founder` bigint(20) unsigned DEFAULT '0' COMMENT '已废弃 创建人id',
  `delete_time` int(10) unsigned DEFAULT '0',
  `ip` varchar(255) DEFAULT NULL,
  `area_id` int(6) unsigned DEFAULT '100200' COMMENT '注册地区代码',
  `area_name` varchar(255) DEFAULT NULL COMMENT '地区名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `createtime` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户表';

#
# Data for table "user"
#

INSERT INTO `user` VALUES (10101555674757204,'admin','ec15d79e36e14dd258cfff3d48b73d35','快乐每一天',1,0,1555827530,1555827583,NULL,NULL,0,0,'127.0.0.1',100200,NULL);
