<?php
namespace app\system\model;
class AdminFunc extends \app\common\model\AdminFunc {

	public function adminUser() {
		return $this->hasOne('admin_user', 'rank_id', 'rank_id');
	}
	public function AdminRankFunc() {
		return $this->hasOne('admin_rank_func', 'func_id', 'id')
			->bind([
				"rank_id" => "rank_id",
				"func_id" => "func_id",
				"relation_id" => "id",
			]);
	}

}