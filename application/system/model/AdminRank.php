<?php
namespace app\system\model;

class AdminRank extends \app\common\model\AdminRank {

	public function adminRankFunc() {
		return $this->hasMany('admin_rank_func', 'rank_id', 'id');
	}
	public function adminUser() {
		return $this->hasMany('adminUser', 'rank_id', 'id');
	}
}