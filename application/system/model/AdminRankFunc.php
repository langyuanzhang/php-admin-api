<?php
namespace app\system\model;

class AdminRankFunc extends \app\common\model\AdminRankFunc {

	public function admin() {
		return $this->hasOne('admin', 'rank_id', 'rank_id');
	}
	public function adminFunc() {
		return $this->hasOne('admin_func', 'id', 'func_id')->bind([
			'action' => 'action',
			'func_name' => 'func_name',
			'method' => 'method',
			'ban_param' => 'ban_param',
			'only_param' => 'only_param',
		]);
	}
	public function adminRank() {
		return $this->hasOne('admin_rank', 'id', 'rank_id')->bind([
			'admin_id' => 'admin_id',
			'rank_name' => 'rank_name',
		]);

	}
	public function add($rank_id, $func_id_arr = []) {
		if (is_array($func_id_arr) && count($func_id_arr)) {
			$list = [];
			foreach ($func_id_arr as $k => $v) {
				$list[] = ['rank_id' => $rank_id, 'func_id' => $v];
			}
			return $this->saveAll($list);
		}
	}
	public function del($rank_id, $func_id_arr = []) {
		if (is_array($func_id_arr) && count($func_id_arr)) {
			$list = [];
			foreach ($func_id_arr as $k => $v) {
				$list[] = $v;
			}
			return $this->where([['rank_id', '=', $rank_id], ['func_id', 'in', $list]])->delete();
		}
	}
}