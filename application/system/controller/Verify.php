<?php
namespace app\system\controller;
use think\captcha\Captcha;
use think\facade\Session;
use yichenthink\utils\ReturnMsg;

class Verify extends Base {

	public $prefix = 'verify';

	public function initialize() {
		parent::initialize();
		Session::init([
			'prefix' => 'admin',
			'type' => '',
			'auto_start' => true,
			'id' => $this->prefix . $this->token['uid'],
		]);
	}
	public function index() {
		$captcha = new Captcha();
		return $captcha->entry();
	}

	public function captcha($id = 'captcha') {
		$captcha = new Captcha();
		$captcha->length = 4;
		$captcha->useNoise = false;
		$captcha->fontSize = 25;
		return $captcha->entry($id);

	}
	public function verifyCaptcha($captcha = '23', $id = 'captcha') {
		// $Cap = new Captcha();
		if (!captcha_check($captcha, $id)) {
			ReturnMsg::returnMsg(400, '验证码有误' . $captcha, $this->token);
			// 验证失败
		}
		return true;
	}

}
