<?php
namespace app\system\controller;
use app\system\model\SystemSafeUrl as Mod;
use app\system\model\User as UserModel;
use yichenthink\utils\ReturnMsg;

class SystemSafeUrl extends AdminBase {

	public function list($p = 0, $trashed = null, $access = 'system') {
		$message = '';
		$code = 400;
		$num = 100;
		// 查找页数
		$startNum = $num * $p;
		$access = !empty($access) ? $access : 'system';
		// 查询被删除的
		if ($trashed) {
			$data = Mod::onlyTrashed()
				->where('access', $access)
				->limit($startNum, $num)
				->select()
				->visible(['id', 'url', 'state', 'content', 'name', 'access']);
		} else {
			// 查询未删除的
			$data = Mod::where('access', $access)->limit($startNum, $num)
				->select()
				->visible(['id', 'url', 'state', 'content', 'name', 'access']);
		}
		if ($data != Null && count($data) > 0) {
			$code = 200;
			$message = '成功';
		} else {
			$message = '没找到数据';
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}
// 修改
	public function add($form = []) {
		//设置允许修改的字段
		$upfield = ['state' => 'state', 'url' => 'url', 'content' => 'content', 'name' => 'name', 'access' => 'access'];
		$message = '';
		$Mod = new Mod;

		foreach ($upfield as $key => $value) {
			# code...
			if (isset($form[$key]) && $form[$key] !== '') {
				# code...
				$Mod->$value = $form[$value];
			}
		}
		$R = $Mod->save();
		if (false !== $R) {

			$code = 200;
		} else {
			$message = '操作失败';
			$code = 400;
		}

		ReturnMsg::returnMsg($code, $message, $Mod);
	}
	// 修改
	public function update($id, $form = []) {

		// ReturnMsg::returnMsg($code = 400, $message = '成功', $form);
		//设置允许修改的字段
		$upfield = ['state' => 'state', 'url' => 'url', 'content' => 'content', 'name' => 'name', 'access' => 'access'];
		$message = '';
		$Mod = Mod::where('id', $id)->find();

		if (null != $Mod) {
			foreach ($upfield as $key => $value) {
				# code...
				if (isset($form[$key]) && $form[$key] != $Mod[$value]) {
					# code...
					$Mod->$value = $form[$value];
				}
			}
			$R = $Mod->save();
			if (false !== $R) {

				$code = 200;
			} else {
				$message = '操作失败';
				$code = 400;
			}
		} else {
			$message = '用户不存在';
			$code = 400;
		}
		ReturnMsg::returnMsg($code, $message, $Mod);
	}
	// 删除一个成员
	public function delete($id = 0, $isTrue = false) {

		$user = Mod::withTrashed()
			->where('id', $id)
			->find();
		$user->delete($isTrue);
		if (0 !== $user) {
			$code = 200;
		} else {
			$code = 400;
		}

		ReturnMsg::returnMsg($code, '', $user);
	}

	//恢复一个成员
	public function restore($id = 0) {
		$num = 0;
		$user = Mod::onlyTrashed()
			->where('id', $id)
			->find();
		# code...
		if ($user) {
			$num++;
			# code...
			$user->restore();
		}
		// $user = UserModel::restore($list);
		ReturnMsg::returnMsg($code = 200, '成功恢复了' . $num . '条数据');
	}

}
