<?php
namespace app;
use think\Controller;
use think\facade\Request;
use think\Session;

class Base extends Controller {
	protected $token = [
		'uid' => '',
		'access_token' => '',
		'time' => '',
	];
	protected $userInfo = [
		'uid' => null,
	];

	protected function initialize() {
		parent::initialize();
		$this->formatToken();
	}
	/**
	 * 把截取前端token进行格式化处理
	 */
	protected function formatToken() {
		$param = Request::param();

		if (isset($param['access_token'])) {
			$token = explode('.', $param['access_token']);
			if (isset($token[2]) && strlen($token[0]) > 10) {
				$this->token = [
					'uid' => $token[0],
					'access_token' => $token[1],
					'time' => $token[2],
				];
				$this->setUserInfo();
			}
		}

	}
	// 如果用户已登陆会生成用户id等信息
	protected function setUserInfo() {
		$this->userInfo['uid'] = $this->token['uid'];

	}
}
