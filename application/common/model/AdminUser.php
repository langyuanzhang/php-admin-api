<?php
namespace app\common\model;
use think\model\concern\SoftDelete;

class AdminUser extends Model {
	use SoftDelete;
	protected $deleteTime = 'delete_time'; //软删除字段
	protected $defaultSoftDelete = 0;
	// 定义时间戳字段名
	protected $createTime = 'create_time';
	protected $updateTime = 'update_time';
	protected $autoWriteTimestamp = true;
	protected $readonly = ['username', 'user_id']; //只读字段不允许修改
	protected $pk = 'user_id';
	// 定义默认值
	protected $auto = ['ip'];
	protected $insert = ['state' => 1];
	protected $update = ['ip'];

}