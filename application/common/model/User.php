<?php
namespace app\common\model;
use think\model\concern\SoftDelete;
use yichenthink\utils\MakeId;

class User extends Model {
	use SoftDelete;
	protected $deleteTime = 'delete_time'; //软删除字段
	protected $defaultSoftDelete = 0;
	// 定义时间戳字段名
	protected $createTime = 'create_time';
	protected $updateTime = 'update_time';
	protected $autoWriteTimestamp = true;
	public $readonly = ['username', 'id']; //只读字段不允许修改

	// 定义默认值
	protected $auto = ['ip'];
	protected $insert = ['state' => 1];
	protected $update = ['ip'];
	protected function setIpAttr() {
		return request()->ip();
	}
	// 关联权限中间模型
	public function rankUser() {
		return $this->hasOne('rank_user', 'user_id', 'id')->bind([
			'rank_id' => 'id',
		]);
	}
	// 关联权限模型
	public function rank() {
		return $this->hasOne('rank', 'id', 'rank_id')->bind([
			'rank_name' => 'name',
		]);
	}
	// 关联状态模型
	public function state() {
		return $this->hasOne('state', 'id', 'state')->bind([
			'state_name' => 'name',
		]);
	}
	// 关联城市模型
	public function area() {
		return $this->hasOne('area', 'id', 'area_id')->bind([
			'city' => 'name',
		]);
	}
	// 生成用户id
	static function makeId() {
		return MakeId::User();
	}
}