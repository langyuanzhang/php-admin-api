<?php
namespace app\common\myconfig;
/**
 *
 */
class Config {
// 模块数据库
	static public $module = [
		'user' => [
			'title' => '用户模块',
			'model' => 'user',
			'template' => 'item',
			'db_config' => 'db_user',
			'dbTable' => '../sql/user.sql',
		],
		'system' => [
			'title' => '基础模块',
			'model' => 'system',
			'template' => 'system',
			'db_config' => 'db_system',
			'dbTable' => '../sql/system.sql',
		],
		'news' => [
			'title' => '内容模块',
			'model' => 'news',
			'template' => 'item',
			'db_config' => 'db_news',
			'dbTable' => '../sql/news.sql',
		],

	];
	// 模型对应数据库配置连接
	static public $db_connection = [
		'Ad' => 'db_system',
		'AdImage' => 'db_system',
		'AdCategory' => 'db_system',
		'Admin' => 'db_system',
		'AdminFunc' => 'db_system',
		'AdminGroup' => 'db_system',
		'AdminGroupFunc' => 'db_system',
		'AdminUser' => 'db_system',
		'AdminRank' => 'db_system',
		'AdminRankFunc' => 'db_system',

		'Area' => 'db_system',
		'DeleteTable' => 'db_system',
		'Nav' => 'db_system',
		'SystemSafe' => 'db_system',
		'SystemSafeGrade' => 'db_system',
		'SystemSafeUrl' => 'db_system',

		'News' => 'db_news',
		'NewsCategory' => 'db_news',
		'NewsContent' => 'db_news',
		'NewsImage' => 'db_news',

		'User' => 'db_user',
	];
}