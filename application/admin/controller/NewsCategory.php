<?php
namespace app\admin\controller;
use app\admin\model\NewsCategory as Mod;
use app\admin\validate\NewsCategory as NewsCategoryValidate;
use think\facade\Request;
use yichenthink\utils\FileBase64;
use yichenthink\utils\ReturnMsg;

class NewsCategory extends Base {
	// 查询系统分类列表

	public function list() {
		$message = '没有数据';
		$code = 400;
		// 查询未删除的
		$data = Mod::select();
		if ($data) {
			$code = 200;
			$message = '成功';
		}
		ReturnMsg::returnMsg($code, $message, $data->visible(['id', 'ranking', 'name', 'image', 'url', 'pid', 'state', 'genealogy']));
	}

	public function add() {
		$form = $this->param;
		//设置允许修改的字段
		$message = '';
		$code = 400;

		$validate = new NewsCategoryValidate;

		if (!$validate->scene('add')->check($form)) {
			ReturnMsg::returnMsg($code, $validate->getError(), $form);
		}
		$form['user_id'] = $this->userInfo['uid'];
		$FileBase64 = new FileBase64;
		//获取base64流的信息
		if ($FileBase64->getBase64Info($form['image'])) {
			// 预备上传 返回图片上传后的信息 路径名 图片类型，尺寸等信息
			$FileBase64->readyUpload('news/' . date('y/m/d/'));
			//获取图片地址
			$form['image'] = $FileBase64->url;

		} else {
			$form['image'] = 0;
		};
		// 上级族谱必须是数组
		if (isset($form['genealogy'])) {

			if (!is_array($form['genealogy'])) {
				$form['genealogy'] = [0];
			}
			// 上级id=族谱最后一个值 //防止被恶意修改
			$form['pid'] = $form['genealogy'][count($form['genealogy']) - 1];
			$form['genealogy'] = json_encode($form['genealogy']);
		} else {
			$form['pid'] = 0;
			$form['genealogy'] = "[]";
		}
		$Mod = new Mod;
		// ReturnMsg::returnMsg($code, $message, $form);
		if ($Mod->allowField(['state', 'user_id', 'pid', 'image', 'name', 'ranking', 'genealogy'])->save($form)) {
			//开始上传
			if (!$FileBase64->upload()) {
				$message = '发布成功，但图片上传失败';
			}
			$code = 200;
		}
		ReturnMsg::returnMsg($code, $message, $Mod);
	}
	public function update() {
		$form = $this->param;
		//设置允许修改的字段
		$message = '';
		$code = 400;

		$validate = new NewsCategoryValidate;

		if (!$validate->check($form)) {
			ReturnMsg::returnMsg($code, $validate->getError(), $form);
		}
		$Mod = Mod::where('id', $form['id'])->find();
		$isUpImg = false;
		if (isset($form['image']) && $Mod->image != $form['image']) {
			$FileBase64 = new FileBase64;
			//获取base64流的信息
			if ($FileBase64->getBase64Info($form['image'])) {
				// 预备上传 返回图片上传后的信息 路径名 图片类型，尺寸等信息
				$FileBase64->readyUpload('news/' . date('y/m/d/'));
				//获取图片地址
				$form['image'] = $FileBase64->url;
				$FileBase64->destroy($Mod->image);
				$isUpImg = true;
			} else {
				$form['image'] = $Mod->image;
			};
		}
		// 上级族谱必须是数组
		if (isset($form['genealogy'])) {

			if (!is_array($form['genealogy'])) {
				$form['genealogy'] = [0];
			}
			// 上级id=族谱最后一个值 //防止被恶意修改
			$form['pid'] = $form['genealogy'][count($form['genealogy']) - 1];
			$form['genealogy'] = json_encode($form['genealogy']);
		}

		$form['user_id'] = $this->userInfo['uid'];
		if ($Mod->allowField(['state', 'pid', 'image', 'name', 'ranking', 'genealogy'])->save($form)) {
			//开始上传
			if ($isUpImg && !$FileBase64->upload()) {
				$message = '发布成功，但图片上传失败';
			}
			$code = 200;
		}
		ReturnMsg::returnMsg($code, $message, $Mod->visible(['id', 'ranking', 'name', 'image', 'url', 'pid', 'state', 'genealogy']));
	}
	// 删除一条
	public function delete($id = 0) {
		$isTrue = true; //默认彻底删除

		$map[] = ['pid', '=', $id];
		$code = 400;
		$map[] = ['user_id', '=', $this->userInfo['uid']];
		$Mod = Mod::withTrashed()->where($map)
			->find();

		$message = '';
		if ($Mod) {
			$message = '请删除子内容后在进行操作';
		} else {
			$map2[] = ['id', '=', $id];
			$map2[] = ['user_id', '=', $this->userInfo['uid']];
			$Mod = Mod::withTrashed()->where($map2)
				->find();
			if ($Mod) {
				if (isset($Mod->image) && !empty($Mod->image)) {
					// 取出图片
					$FileBase64 = new FileBase64;
					// 删除图片资源
					$FileBase64->destroy($Mod->image);
				}

				if ($isTrue) {
					$Mod->delete($isTrue);
					# code...
				} else {
					$Mod->delete($isTrue);
				}
				$code = 200;
			}

		}

		ReturnMsg::returnMsg($code, $message, $Mod);
	}
}
