<?php
namespace app\admin\validate;

use think\Validate;

class AdCategory extends Validate {
	protected $rule = [
		'name|名称' => 'min:2|max:32',
		'state|状态' => 'number|between:0,2',
		'pid|父id' => 'number',
		'image|图片' => 'min:102|max:4088000|regex:/^data:((\w+)\/(\w+));base64,/',
	];
	protected $message = [
		'image.regex' => '图片格式有误',
	];

	// edit 验证场景定义
	public function sceneAdd() {
		return $this->append('name', 'require')
			->append('image', 'require')
			->append('state', 'require');
	}
	// // edit 验证场景定义
	// public function sceneEdit() {
	// 	return $this->append('title', 'require')
	// 		->append('content', 'require')
	// 		->append('image', 'require');
	// }
}
?>