<?php
namespace app\install\controller;
use think\facade\Request;
use yichenthink\utils\ReturnMsg;

class Base extends \app\Base {
	protected $inlet = "/install.php"; //放行的支持入口文件
	// 禁止 使用其他入口文件进入.. 仅支持install.php 作为入口文件
	protected function initialize() {
		parent::initialize();
		$path = Request::server()['SCRIPT_NAME'];
		// echo substr_compare($path, $this->inlet, -strlen($this->inlet));
		// 判断入口文件是否是访问域名的结尾
		if (substr_compare($path, $this->inlet, -strlen($this->inlet)) !== 0) {

			ReturnMsg::returnMsg(400, "非法请求,请使用，http://" . Request::host() . $this->inlet . "进行访问", $path);
		}
	}
}
