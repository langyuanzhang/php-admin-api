<?php
namespace app\home\model;

class DeleteTable extends \app\common\model\DeleteTable {

	// 关联内容模型
	public function newsContent() {
		return $this->hasOne('NewsContent', 't_id', 't_id')->bind([
			'title' => 'title',
			'content' => 'content',
			'image' => 'image',
		]);
	}
}